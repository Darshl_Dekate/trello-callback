//Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from 
//the given data in cards.json. Then pass control back to the code that called it by using a callback function.

const cards = require('./cards.json')


function dataCard(listID, cards) {
    let keys = Object.keys(cards);   
    let result = keys.reduce((res,each)=>{
        if(each==listID)
        {
            res.push(cards[each])
        }

        return res;
    },[])

    console.log(result)
}

function cardInformation(callback, listID) {
   
    setTimeout(() => {
        callback(listID, cards)
        console.log("Control Back to Code")

    }, 2000)

}
module.exports = { cardInformation, dataCard }