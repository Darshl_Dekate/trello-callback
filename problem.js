// //Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the 
//given list of boards in boards.json 
// //and then pass control back to the code that called it by using a callback function.




const board = require("./boards.json")


function callback(boardID, board) {
    //console.log(board)
    const boardInfo = board.reduce((acc, curr) => {
        if (curr["id"] == boardID) {
            acc.push(curr)
        }
            return acc;       
    }, []);
    console.log(boardInfo)
}

function boardInformation(callback, boardID) {
   
    setTimeout(() => {
        callback(boardID, board)
       
    }, 2000)

}
module.exports = { boardInformation, callback }




